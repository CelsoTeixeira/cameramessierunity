using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text CurrentCameraText; //We will make this public, if its null we will find the game object and get the componenet.

    public Text FrozenCameraText;   //Our text when the current camera its frozen.

    public GameObject AdvancedPanel;

    private List<Button> _advancedPanelButtons = new List<Button>();

    private string _staticCurrentCameraText = "Current camera target: ";

    private CameraManager _cameraManager;

    private int x = 1;

    void Awake()
    {
        if (_cameraManager == null)
            _cameraManager = GameObject.FindGameObjectWithTag(TagHelper.GeneralManager).GetComponent<CameraManager>();

        if (CurrentCameraText == null)
            CurrentCameraText = GameObject.FindGameObjectWithTag(TagHelper.CameraTargetTextTag).GetComponent<Text>();

        if (FrozenCameraText == null)
            FrozenCameraText = GameObject.FindGameObjectWithTag(TagHelper.FrozenCameraText).GetComponent<Text>();
        
        GetAdvancedPanelList(); //We get our references.
    }

    void Start()
    {
        CurrentCameraText.text = _staticCurrentCameraText + (_cameraManager.GetCurrentCamera().gameObject.name);

        SetupButtonsText();     //We modify the buttons text and onClick.

        AdvancedPanel.SetActive(false);     //We deactive the advanced panel.

        Cursor.visible = false;
    }

    void Update()
    {
        if (_cameraManager.GetSpecifiCameraBase(_cameraManager.CurrentCamera).IsFrozen)
        {
            if (!FrozenCameraText.gameObject.activeInHierarchy)
            {
                FrozenCameraText.gameObject.SetActive(true);
                Cursor.visible = true;
            }
        }
        else
        {
            if (FrozenCameraText.gameObject.activeInHierarchy)
            {
                FrozenCameraText.gameObject.SetActive(false);
                Cursor.visible = false;
            }
        }
    }


    public void TogglePanel(GameObject goToToggle)
    {
        goToToggle.SetActive(!goToToggle.activeInHierarchy);
    }

    //Loop to get all the references we need for the advanced panel.
    private void GetAdvancedPanelList()
    {
        Button[] buttons = AdvancedPanel.GetComponentsInChildren<Button>();

        for (int x = 0; x < buttons.Length; x++)
        {
            _advancedPanelButtons.Add(buttons[x]);
        }
    }

    //Loop to setup the names and methods on every button.
    private void SetupButtonsText()
    {
        for (int x = 0; x < _advancedPanelButtons.Count; x++)
        {
            Text t = _advancedPanelButtons[x].GetComponentInChildren<Text>();   //Our text to change.

            string name = _cameraManager.CameraGameObjects[x].name;     //The name of the object.

            t.text = "Go to " + name;   //Refresh the button text.

            AddOnClickListener(_advancedPanelButtons[x], x);    //Add the method to the onCLick button.
        }
    }

    //Need to do this because of a c# unity version bug...
    private void AddOnClickListener(Button b, int cameraToGo)
    {
        b.onClick.AddListener(() => _cameraManager.ChangeToSpecifiCamage(cameraToGo));
    }

#region Public methods

    /// <summary>
    /// GoToNextCamera and GoToPreviousCamera its to be called from the Buttons in the UI.
    /// </summary>
    public void GoToNextCamera()
    {
        _cameraManager.ChangeCameraTarget(+x);  //Positive value to go to the next value in the list.
    }

    public void GoToPreviousCamera()
    {
        _cameraManager.ChangeCameraTarget(-x);  //Negative value to go to the previous value in the list.
    }

    //If we need to force text refresh from somewhere we can just call this and pass the text update.
    public void ForceTextRefresh()
    {
        CurrentCameraText.text = _staticCurrentCameraText + (_cameraManager.GetCurrentCamera().gameObject.name);    //Refresh our ui text.
    }


#endregion

}