﻿using UnityEngine;
using System.Collections;

public class InputControl : MonoBehaviour
{
    public KeyCode KeyCodeToCycleCameras = KeyCode.Tab;

    private CameraManager _cameraManager;

    void Awake()
    {
        _cameraManager = GameObject.FindGameObjectWithTag(TagHelper.GeneralManager).GetComponent<CameraManager>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCodeToCycleCameras))
        {
            _cameraManager.ChangeCameraTarget(+1);
        }
    }

}