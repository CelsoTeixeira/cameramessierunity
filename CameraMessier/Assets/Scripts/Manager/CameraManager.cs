using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CameraManager : MonoBehaviour
{
    //We can make this private.
    public List<GameObject> CameraGameObjects;

    [Tooltip("Se as cameras devem ser colocadas manualmente em CameraGameObjects isto deve ser false. Se for true nos acharemos todas as cameras na scene e adicionaremos na lista.")]
    public bool FindCamerasAutomatic = true;

    //We can make this private.
    public List<CameraBase> _myCameraBases = new List<CameraBase>();

    private int currentCamera = 0;  //Our current camera.

    public int CurrentCamera { get { return currentCamera; } }

    private UIManager _uiManager;

    void Awake()
    {
        _uiManager = GameObject.FindGameObjectWithTag(TagHelper.GeneralManager).GetComponent<UIManager>();

        if (FindCamerasAutomatic)   //Will just find all the cameras objects and populate our list.
        {
            CameraGameObjects = new List<GameObject>();
            CameraGameObjects.Add(GameObject.FindGameObjectWithTag(TagHelper.StaticTag));

            GameObject[] personagens = GameObject.FindGameObjectsWithTag(TagHelper.PersonagemTag);

            foreach (GameObject go in personagens)
            {
                CameraGameObjects.Add(go);
            }
        }

        //Just a safe check here to show where we can have a error and how to solve it.
        if (CameraGameObjects.Count < 3)
            Debug.Log("Nos nao temos o minimo(3) necessario de cameras para funcionar adequadamente.", this.gameObject);

        for (int x = 0; x < CameraGameObjects.Count; x++)           //Simply loop to get all the CameraBase reference we will need.
        {
            _myCameraBases.Add(CameraGameObjects[x].GetComponent<CameraBase>());
        }
    }

    void Start()
    {
        InitialSetup();
    }
    
    /// <summary>
    /// We make sure the currentCamera its 0, so we start with the cenarioCamera.
    /// We deactive every single camera with a loop.
    /// We active the first camera inside our list.
    /// </summary>
    private void InitialSetup()
    {
        currentCamera = 0;  //This is the current camera in our list.

        for (int x = 0; x < _myCameraBases.Count; x++)
        {
            _myCameraBases[x].DeActiveCamera();     //Deactive all the cameras.
        }

        _myCameraBases[currentCamera].ActiveCamera();   //ACtive only our current camera.
    }

    /// <summary>
    /// We request an int just and this valor should be +1 or -1,
    /// with this value we cycle inside our _myCameraBases and deactive and active the
    /// cameras and we refresh the text in the ui too.
    /// </summary>
    public void ChangeCameraTarget(int x)
    {
        x = Mathf.Clamp(x, -1, 1);  //Make sure we only have a maximun of 1 unit to increase or decrease.

        _myCameraBases[currentCamera].DeActiveCamera(); //DeActive the current camera.

        currentCamera += x;                               //Increase the current camera.

        if (currentCamera > _myCameraBases.Count - 1)      //Make sure we stay inside the size of our list of cameras.
            currentCamera = 0;
        else if (currentCamera < 0)
            currentCamera = _myCameraBases.Count - 1;


        _myCameraBases[currentCamera].ActiveCamera();   //Active the new current camera.

        _uiManager.ForceTextRefresh();      //Force a text refresh on the UI.
    }
    
    /// <summary>
    /// We disable the actual camera and change to the 
    /// camera number passed by X.
    /// </summary>
    public void ChangeToSpecifiCamage(int x)
    {
        _myCameraBases[currentCamera].DeActiveCamera();     //Deactive camera.

        currentCamera = x;      //update our counter.

        _myCameraBases[currentCamera].ActiveCamera();   //Active the new current camera.

        _uiManager.ForceTextRefresh();      //Force a text refresh on the UI.
    }

    public CameraBase GetSpecifiCameraBase(int x)
    {
        x = Mathf.Clamp(x, 0, _myCameraBases.Count - 1);

        return _myCameraBases[x];
    }

    public GameObject GetSpecificCamera(int x)
    {

        x = Mathf.Clamp(x, 0, CameraGameObjects.Count - 1);

        return CameraGameObjects[x];
    }

    public GameObject GetCurrentCamera()
    {
        return CameraGameObjects[currentCamera];
    }

}