using UnityEngine;
using System.Collections;

public class FirstPersonCamera : CameraBase
{
    public float MouseSensitivity = 2f;
    public float VerticalMinRange = -60f;
    public float VerticalMaxRange = 60f;

    private float _horizontal = 0;
    private float _vertical = 0;

    private bool _isWorking = true;

    void Update()
    {
        if (!CameraActive || MyCameraGameObject == null)
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _isWorking = !_isWorking;
            _isFrozen = !_isFrozen;
        }
    }

    /// <summary>
    /// Simple method to deal with camera rotations.
    /// This Updates after the Update method.
    /// The first thing we want to check it's to see if we are Active by the cameraManager and if we have a Camera object, 
    /// if not, we just leave and don't do anything.
    /// For the horizontal rotation, we want to rotate the object in the Y axe.
    /// For the vertical rotation we want to rotate the camera object in the X axe.
    /// We clamp the _vertical to be a min/max amount that can be setted in the Editor.
    /// </summary>
    void LateUpdate()
    {
        if (!CameraActive || MyCameraGameObject == null)
            return;
        
        if (!_isWorking ||_isFrozen)
            return;

        _horizontal = Input.GetAxis("Mouse X") * MouseSensitivity;
        this.transform.Rotate(0, _horizontal, 0);
        
        _vertical -= Input.GetAxis("Mouse Y") * MouseSensitivity;
        _vertical = Mathf.Clamp(_vertical, VerticalMinRange, VerticalMaxRange);
        MyCameraGameObject.transform.localRotation = Quaternion.Euler(_vertical, 0, 0);
    }
}