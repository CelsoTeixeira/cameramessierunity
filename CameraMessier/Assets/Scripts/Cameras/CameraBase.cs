using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for our cameras with some basic
/// functionality that can be override if needeed.
/// </summary>
public class CameraBase : MonoBehaviour
{
    public GameObject MyCameraGameObject;
  
    protected bool CameraActive = false;    //If the camera it's active or not.

    protected bool _isFrozen = false;
    public bool IsFrozen { get { return _isFrozen; } }

    //The manager will check for any other camera to make sure we only have one active camera at time.
    public virtual void ActiveCamera()
    {
        CameraActive = true;
        MyCameraGameObject.SetActive(true);    
    }

    public virtual void DeActiveCamera()
    {
        CameraActive = false;
        MyCameraGameObject.SetActive(false);
    }
}