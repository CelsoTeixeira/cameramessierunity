using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StaticCamera : CameraBase
{
    public List<GameObject> Personagens;

    public bool LookAtMidPoint = true;

    void Awake()
    {
        Personagens = GameObject.FindGameObjectsWithTag(TagHelper.PersonagemTag).ToList();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _isFrozen = !_isFrozen;
        }
    }

    /// <summary>
    /// If LookAtMidPoint its true, we get the midPoint between the two Personagens
    /// and make the Camera look at it.
    /// TODO - We can still lose focus, so we add walls to prevent this, WE CANT MOVE THE CAMERA! Otherwise we could play with the Y position, increasing and decreasing it.
    /// </summary>
    void LateUpdate()
    {
        if(!CameraActive)
            return;

        if (LookAtMidPoint)
        {
            MidPoint();
        }
    }

    private void MidPoint()
    {
        Vector3 midPoint = (Personagens[0].transform.position + Personagens[1].transform.position) / 2;

        MyCameraGameObject.transform.LookAt(midPoint);

#if UNITY_EDITOR

        Debug.DrawLine(MyCameraGameObject.transform.position, midPoint, Color.yellow);

        Debug.DrawLine(Personagens[0].transform.position, Personagens[1].transform.position, Color.red);

#endif        
    }
}