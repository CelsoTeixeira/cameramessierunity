using UnityEngine;
using System.Collections;

public static class TagHelper
{
    public static string PersonagemTag = "Personagem";
    public static string StaticTag = "CameraStatic";

    public static string CameraTargetTextTag = "CurrentCameraText";
    public static string FrozenCameraText = "FrozenCameraText";

    public static string GeneralManager = "Manager";


}